/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <string.h>
#include <stdio.h>

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI2_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/**
 **************************************************************************************************************
 *
 * @author Marco Pérez
 * @brief The main function of the following class is to read and write information in the ADXL_357 sensor, on SPI and I2C.
 *
 **************************************************************************************************************
 */

class ADXL356_Driver{
	public:
	/**
	 * @brief	Function to read information by SPI
	 * @param	hspi	Interface's instance
	 * @param	REG		Register of the wanted information
	 * @param	pData	Variable to store all the data received
	 * @retval	HAL_StatusTypeDef	Status of the interface
	 */
	HAL_StatusTypeDef SPI_readSensor(SPI_HandleTypeDef hspi, uint8_t REG, uint8_t *pData, uint16_t SS_PIN){
	   HAL_StatusTypeDef returnValue; /**< Variable to store the interface's status */

	   //Create a buffer and equal the first value to the register
	   uint8_t buff[4];
	   buff[0] = REG;

	   //Select the slave and start the communication
	   HAL_GPIO_WritePin(GPIOA, SS_PIN, GPIO_PIN_RESET);
	   //Send the register to the sensor
	   returnValue = HAL_SPI_Transmit(&hspi, buff, 1, HAL_MAX_DELAY);

	   //Check if an error has occurred during the transaction
	   if(returnValue != HAL_OK)
	       return returnValue;

	   //End by receiving the wanted information and storing it
	   returnValue = HAL_SPI_Receive(&hspi, buff, sizeof(pData)+1, HAL_MAX_DELAY);

	   //Finish the connection with the with the sensor
	   HAL_GPIO_WritePin(GPIOA, SS_PIN, GPIO_PIN_SET);

	   //Copy the data to the variable desired and erase the buffer
	   memcpy(pData, buff, sizeof(pData)+1);
	   memset(buff, 0, sizeof(pData)*sizeof(uint8_t));

	   //Return the interface's status
	   return returnValue;
	}

	/**
	 * @brief	Function to write information in the sensor's registers by SPI
	 * @param	hspi	Interface's instance
	 * @param	REG		Register where to rewrite the information
	 * @param	pData	Variable where all the data is stored
	 * @retval	HAL_StatusTypeDef	Status of the interface
	 */
	HAL_StatusTypeDef SPI_writeSensor(SPI_HandleTypeDef hspi, uint8_t REG, uint8_t *pData, uint16_t SS_PIN){

	   HAL_StatusTypeDef returnValue;	/**< Variable to store the interface's status */
	   uint8_t data[4];	/**< Buffer to store all the data we want to send */

	   //Set the first byte of the array equal to the register and the rest to the data
	   data[0] = REG;
	   memcpy(data+1, pData, sizeof(pData)+1);

	   //Select the slave and start the communication
	   HAL_GPIO_WritePin(GPIOA, SS_PIN, GPIO_PIN_RESET);

	   //Send all the buffer to the sensor
	   returnValue = HAL_SPI_Transmit(&hspi, data, sizeof(pData), HAL_MAX_DELAY);

	   //Check if it was done correctly
	   if(returnValue != HAL_OK)
	    	return returnValue;

	   //Finish the connection with the with the sensor
	   HAL_GPIO_WritePin(GPIOA, SS_PIN, GPIO_PIN_SET);

	   //Clean the buffer after sending the message correctly
	   memset(data, 0, sizeof(uint8_t)*sizeof(data));

	   //Return the HAL_OK value as a sign that everything went well
	   return HAL_OK;
	}

	/**
	 * @brief	Function to read information by I2C
	 * @param	hi2c	Interface's instance
	 * @param	REG		Register of the wanted information
	 * @param	pData	Variable to store all the data received
	 * @retval	HAL_StatusTypeDef	Status of the interface
	 */
	HAL_StatusTypeDef I2C_readSensor(I2C_HandleTypeDef hi2c, uint8_t DevAddress, uint8_t REG, uint8_t *pData){
		HAL_StatusTypeDef returnValue; /**< Variable to store the interface's status */

		//Create a buffer and equal the first value to the register
		uint8_t buff[4];
		buff[0] = REG;

		//Send the register to the sensor
		returnValue = HAL_I2C_Master_Transmit(&hi2c, DevAddress, buff, 1, HAL_MAX_DELAY);

		//Check if an error has occurred during the transaction
		if(returnValue != HAL_OK)
		    return returnValue;

		//End by receiving the wanted information and storing it
		returnValue = HAL_I2C_Master_Receive(&hi2c, DevAddress, buff, sizeof(pData)+1, HAL_MAX_DELAY);

		//Copy the data to the variable desired and erase the buffer
		memcpy(pData, buff, sizeof(pData)+1);
		memset(buff, 0, sizeof(pData)*sizeof(uint8_t));

		//Return the interface's status
		return returnValue;
	}

	/**
	 * @brief	Function to write information in the sensor's registers by I2C
	 * @param	hspi	Interface's instance
	 * @param	REG		Register where to rewrite the information
	 * @param	pData	Variable where all the data is stored
	 * @retval	HAL_StatusTypeDef	Status of the interface
	 */
	HAL_StatusTypeDef I2C_writeSensor(I2C_HandleTypeDef hi2c, uint8_t DevAddress, uint8_t REG, uint8_t *pData){
		HAL_StatusTypeDef returnValue; /**< Variable to store the interface's status */
		uint8_t data[4]; /**< Buffer to store all the data we want to send */

		//Set the first byte of the array equal to the register and the rest to the data
		data[0] = REG;
		memcpy(data+1, pData, sizeof(pData)+1);

		//Send all the buffer to the sensor
		returnValue = HAL_I2C_Master_Transmit(&hi2c, DevAddress, data, sizeof(pData), HAL_MAX_DELAY);

		//Check if it was done correctly
		if(returnValue != HAL_OK)
		    return returnValue;

		//Clean the buffer after sending the message correctly
		memset(data, 0, sizeof(uint8_t)*sizeof(data));

		//Return the HAL_OK value as a sign that everything went well
		return HAL_OK;
	}
};

/**
 **************************************************************************************************************
 *
 * @brief The next class is going to use the previous one to read all the information from the sensor, in either interface. It will
 * 		  also process the data obtained.
 *
 **************************************************************************************************************
 */
class AccelerometerInterface{
    private:
		uint8_t Xdata[3]; /**< Buffer to store the obtained data of the X axis */
		uint8_t Ydata[3]; /**< Buffer to store the obtained data of the Y axis */
		uint8_t Zdata[3]; /**< Buffer to store the obtained data of the Z axis */
		uint8_t Tempdata[2]; /**< Buffer to store the obtained data of the temperature */
		float xValue; /**< Variable to store the processed data of the X axis */
		float yValue; /**< Variable to store the processed data of the Y axis */
		float zValue; /**< Variable to store the processed data of the Z axis */
		float tempValue; /**< Variable to store the processed data of the temperature */
		uint8_t DevAddress = 0x1D; /**< Device Address for the I2C communication */
		uint8_t REG_TEMP = 0x06; /**< Register to access the temperature data */
		uint8_t REG_X = 0x08; /**< Register to access the X axis data */
		uint8_t REG_Y = 0x0B; /**< Register to access the Y axis data */
		uint8_t REG_Z = 0x0E; /**< Register to access the Z axis data */
		uint8_t REG_FIFO = 0x11; /**< Register to access the FIFO */
		ADXL356_Driver driver; /**< Instance of the ADXL356_Driver class */
		uint16_t SS_PIN = GPIO_PIN_5; /**<Pin chosen to be the Slave Selector */
	public:
		/**
		 * @brief	Function to read all the information by SPI
		 * @param	hspi	Interface's instance
		 * @retval	HAL_StatusTypeDef	Status of the interface
		 */
		HAL_StatusTypeDef SPI_readData(SPI_HandleTypeDef hspi){
			HAL_StatusTypeDef returnValueX, returnValueY, returnValueZ, returnValueTemp; /**< Variables to store the interface's status */

			//Initialize the SS pin
			initSS_Pin();

			//Read all the sensor's data and store the values of each interface's status
			returnValueX = driver.SPI_readSensor(hspi, REG_X, Xdata, SS_PIN);
			returnValueY = driver.SPI_readSensor(hspi, REG_Y, Ydata, SS_PIN);
			returnValueZ = driver.SPI_readSensor(hspi, REG_Z, Zdata, SS_PIN);
			returnValueTemp = driver.SPI_readSensor(hspi, REG_TEMP, Tempdata, SS_PIN);

			//Process all the obtained data and store it inside the class
			xValue = processAxisInfo(Xdata);
			yValue = processAxisInfo(Ydata);
			zValue = processAxisInfo(Zdata);
			tempValue = processTemp(Tempdata);

			//Return all the different interface's status
			if(returnValueX == HAL_OK && returnValueY == HAL_OK && returnValueZ == HAL_OK && returnValueTemp == HAL_OK)
				return HAL_OK;
			else
				return HAL_TIMEOUT;
		}

		/**
		 * @brief	Function to read all the information by I2C
		 * @param	hspi	Interface's instance
		 * @retval	HAL_StatusTypeDef	Status of the interface
		 */
		HAL_StatusTypeDef I2C_readData(I2C_HandleTypeDef hi2c){
			HAL_StatusTypeDef returnValueX, returnValueY, returnValueZ, returnValueTemp; /**< Variables to store the interface's status */

			//Read all the sensor's data and store the values of each interface's status
			returnValueX = driver.I2C_readSensor(hi2c, DevAddress, REG_X, Xdata);
			returnValueY = driver.I2C_readSensor(hi2c, DevAddress, REG_Y, Ydata);
			returnValueZ = driver.I2C_readSensor(hi2c, DevAddress, REG_Z, Zdata);
			returnValueTemp = driver.I2C_readSensor(hi2c, DevAddress, REG_TEMP, Tempdata);

			//Process all the obtained data and store it inside the class
			xValue = processAxisInfo(Xdata);
			yValue = processAxisInfo(Ydata);
			zValue = processAxisInfo(Zdata);
			tempValue = processTemp(Tempdata);

			//Return all the different interface's status
			if(returnValueX == HAL_OK && returnValueY == HAL_OK && returnValueZ == HAL_OK && returnValueTemp == HAL_OK)
				return HAL_OK;
			else
				return HAL_TIMEOUT;
		}

		/**
		 * @brief	Function to process the obtained data from all the axis
		 * @param	data	Raw values from the sensors
		 * @retval	float	Processed data
		 */
		float processAxisInfo(uint8_t *data){
			uint32_t val; /**< Variable to order the data from the sensor */
			float finalVal; /**< Variable for the processed data */

			//Set all the data in the same variable and check if it is a positive or negative number
			val = ((uint32_t)data[0]<<12 | data[1]<<4 | data[2]>>4);
			if(val > 0x7FFFF){
				val |= 0xFFF00000;
			}

			//Equation deduced from the datasheet
			finalVal = (val / 51.2) - 0.125;

			//Return the processed value
			return finalVal;

		}

		/**
		 * @brief	Function to process the obtained data from the temperature
		 * @param	data	Raw values from the sensor
		 * @retval	float	Processed data
		 */
		float processTemp(uint8_t *data){
			uint16_t val; /**< Variable to order the data from the sensor */
			float finalVal; /**< Variable for the processed data */

			//Set all the data in the same variable and check if it is a positive or negative number
			val = ((uint16_t)data[0]<<4 | data[1]>>4);
			if(val > 0x7FF){
				val |= 0xF000;
			}

			//Equation deduced from the datasheet
			finalVal = ((1885 - val) / 9.05) + 25;

			//Return the processed value
			return finalVal;

		}

		/**
		 * @brief	Function to initialize the Slave Selector pin
		 */
		void initSS_Pin(){
			__HAL_RCC_GPIOA_CLK_ENABLE(); /**< Enable the peripheral clock */

			GPIO_InitTypeDef GPIO_InitStruct; /**< Instance the struct of the peripheral */

			GPIO_InitStruct.Pin = SS_PIN; /**< Pin selected to be the Slave Selector */
			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP; /**< Select the Push Pull mode */
			GPIO_InitStruct.Pull = GPIO_NOPULL; /**< No Pull-Up nor Pull-Down resistor mode */
			GPIO_InitStruct.Speed = GPIO_SPEED_HIGH; /**< Set the fastest speed */
			HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); /**< Finish the initialization */

			HAL_GPIO_WritePin(GPIOA, SS_PIN, GPIO_PIN_SET); /**< Set the pin high by default */
		}

		/**
		 * @brief	Getter of the X axis
		 * @retval	float	Value of the variable
		 */
		float getX(){
			return xValue;
		}

		/**
		 * @brief	Getter of the Y axis
		 * @retval	float	Value of the variable
		 */
		float getY(){
			return yValue;
		}

		/**
		 * @brief	Getter of the Z axis
		 * @retval	float	Value of the sensor
		 */
		float getZ(){
			return zValue;
		}

		/**
		 * @brief	Getter of the temperature
		 * @retval	float	Value of the sensor
		 */
		float getTemp(){
			return tempValue;
		}
};
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint16_t buf[20]; /**< Create the buffer to print the status */
	float xValue, yValue, zValue, tempValue;
	HAL_StatusTypeDef ret; /**<Status of the interface */
	AccelerometerInterface sensor; /**< Object from the AccelerometerInterface class */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI2_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
	  //Call the readData function
	  ret = sensor.I2C_readData(hi2c1);
	  if(ret == HAL_OK){
		  //Store all the data received
		  xValue = sensor.getX();
		  yValue = sensor.getY();
		  zValue = sensor.getZ();
		  tempValue = sensor.getTemp();

		  //Print the X data
		  xValue *= 100;
		  sprintf((char*)buf, "X acceleration: %u.%02u g", (unsigned)xValue/100, (unsigned)xValue%100);

		  //Print the Y data
		  yValue *= 100;
		  sprintf((char*)buf, "Y acceleration: %u.%02u g", (unsigned)yValue/100, (unsigned)yValue%100);

		  //Print the Z data
		  zValue *= 100;
		  sprintf((char*)buf, "Z acceleration: %u.%02u g", (unsigned)zValue/100, (unsigned)zValue%100);

		  //Print the Temperature data
		  tempValue *= 100;
		  sprintf((char*)buf, "Temperature: %u.%02u ºC", (unsigned)tempValue/100, (unsigned)tempValue%100);
	  }
	  else{
		  sprintf((char*)buf, "An error has occurred\n");
	  }
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_1LINE;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
